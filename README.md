# cgi-time-tracker

Training

--------------------------------------------------------------------
git commands 
--------------------------------------------------------------------
Git global setup
git config --global user.name "Issam El Kbaiche"
git config --global user.email "mohamedissamelkbaiche@gmail.com"

Create a new repository
git clone https://gitlab.com/mohamedissamelkbaiche/nouveau-project.git
cd nouveau-project
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/mohamedissamelkbaiche/nouveau-project.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/mohamedissamelkbaiche/nouveau-project.git
git push -u origin --all
git push -u origin --tags